# Cassandra Installation

[TOC]

Leider führt die offizielle Anleitung zur Installation auf Linux (unter Verwendung der Package Manager)  nicht zu einer funktionierenden Instanz. Darum wurde in dieser Installation auf die Verwendung von Docker zurückgegriffen. Sie können Docker natürlich auch auf einer AWS Instanz verwenden - dies überlassen wir Ihrer Entscheidung.

Sie benötigen nur die beiden Befehle um Cassandra als Docker zu installieren

```powershell
docker pull cassandra:latest
docker run --name cassandra -p 9042:9042 -p 9160:9160 -d cassandra:latest
```



Nach der Installation können Sie die Shell von Cassandra über den folgenden Befehl aufrufen. **Warten Sie ein paar Minuten** bevor Sie diesen ausführen, damit die Instanz auch korrekt starten kann. 

```powershell
docker exec -it cassandra cqlsh
```

