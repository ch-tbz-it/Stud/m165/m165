# Cassandra Datenmodellierung

[TOC]

## Grundsätzliche Überlegungen

Die Beschreibungen zu Wide-Column Databases und Column-Family weichen in verschiedenen Quellen voneinander ab. Wir können aber davon ausgehen, dass das Hauptkonzept dazu dient Daten zu Gruppieren (Row-Key) und dann unterschiedliche Produkte unterschiedlichen Implementationen davon ausliefern. 

Da wir uns hier konkret mit Cassandra befassen, werden wir auch dessen Konzepte vertieft anschauen. 



## Gruppierung von Daten

Das Grundkonzept der Gruppierung können wir am einfachsten erklären, wenn wir von relationalen Daten ausgehen und eine wichtige Limitierung anschauen - eben genau die Gruppierung Schauen Sie sich das folgende Bild an.

Wir gehen von zwei Tabellen aus, die man zusammenführen möchte (Joined Tabelle). Sobald man aber aggregierte Daten erstellt, kann man diese in RDBMS nicht mehr darstellen, weil ein Feld nicht über mehrere Zeilen reichen kann.

![join](./x_res/join.png)

Wenn Sie sich nun im Fall oben - im dritten Bild - die Spalte *DepId* als Row-Key einer Wide-Column Datebank vorstellen, ist diese Darstellung auch als Speicherung plötzlich doch möglich. 

Wieso soll man Daten in dieser Form speichern wollen? Es geht um Performance. Ein Join von zwei oder mehreren Tabellen ist extrem teuer, vor allem wenn man dann auch noch filtern möchte. Wenn man die Aggregierten Daten direkt auslesen kann, ist man viel schneller - speziell bei grossen Datenmengen.

Es ist wahr, dass diese Speicherung wahrscheinlich nur genau für einen Use-Case nützlich ist und für andere Use Cases nicht. Bei Wide-Column Datenbanken (bei uns Cassandra) ist es auch so gedacht, dass man **pro Abfrage** eigene Speichergruppen erstellt. Es ist ziemlich sicher, dass Daten so mehrfach gespeichert werden - dies ist explizit erwünscht! Redundanzen werden so eingeführt.



## Partitionierung und Keys

Wenn Sie sich später in die Modellierung von Cassandra einlesen, werden Sie auf die folgenden Begriffe stossen, die wir mit dem folgenden Bild bereits erklären:

Die Werte im roten Bereich werden in Cassandra Partition Key genannt. Eine Partition wird immer zusammen auf einem Node gespeichert und nicht getrennt. So kann man hohe Performance sicherstellen. Es können mehrere Partition Keys existieren.

Die Werte im grünen Bereich sind die Cluster Keys. Die werden genutzt für die Sortierung und Identifizierung der Einträge innerhalb der Partition.  Es muss nur dann Cluster Keys geben, wenn die Partitionierung mehrere Datensätzen enthält. Es kann auch mehrere Cluster Keys geben.

Der Primary Key ist die Kombination der Partition Keys und der Cluster Keys.

![concepts](./x_res/concepts.png)



## Datenmodellierung

Die Webseite erklärt sehr gut, wie Sie vom [konzeptionellen](https://cassandra.apache.org/doc/latest/cassandra/developing/data-modeling/data-modeling_conceptual.html) zum [logischen](https://cassandra.apache.org/doc/latest/cassandra/developing/data-modeling/data-modeling_logical.html) dann zum [physischen](https://cassandra.apache.org/doc/latest/cassandra/developing/data-modeling/data-modeling_physical.html) Modell gelangen - daher werden wir hier darauf verzichten weitere Beispiele zu nennen. Ich möchte erwähnen, dass das konzeptionelle Modell im Link oben einen Fehler aufweist. Zwischen Room und A.. ist eine N:N Beziehung und keine 1:N Beziehung. Dies ist dann im logischen Modell sichtbar. 

Die Grundschritte zur Modellierung sind:

- Erstellen Sie ein konzeptionelles Modell. Das haben Sie ja bereits
- Überlegen Sie sich nun, welche Abläufe und welche Inhalte für einen Besucher sichtbar sind. Dieser Schritt ist wichtig, da Sie ja Ihre Abfragen optimieren möcht<en. Sie müssen sich also viel mehr um das Screen-Design kümmern als um die Datenbank. 
- Erstellen Sie für alle Ihre Screens das logische und dann auch das physische Datenmodell.



## Tools

[DataGrip](https://www.jetbrains.com/datagrip/): Graphisches Tool, welches Cassandra unterstützt.

[cqlsh unter Python](https://pypi.org/project/cqlsh/): Falls Sie sich nicht mit cqlsh auf dem Docker Container verbinden möchten, können Sie sich für Windows das cqlsh Tool herunterladen.



## Weitere Quellen

- <https://cassandra.apache.org/doc/latest/cassandra/developing/data-modeling/index.html>
- <https://www.baeldung.com/cassandra-keys>
