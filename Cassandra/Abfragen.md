# Cassandra Abfragen, Datenmanipulation

[TOC]

## Datendefinition (DDL)

Die Befehle für die [Datendefinition](https://cassandra.apache.org/doc/latest/cassandra/developing/cql/ddl.html) haben ähnliche Syntax wie SQL.

Während in SQL Daten in Schemas und/oder Datenbanken separiert werden, heisst dies in Cassandra *Keyspace*. Sie müssen immer zuerst einen Keyspace erstellen und diesen auswählen bevor Sie Tabellen anlegen können.  Sie können im Link oben nachlesen was die einzelnen Anweisungen im Befehl bedeuten.

```cassandra
CREATE KEYSPACE yourNameForThisKeyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 1};
```

Nachdem Sie den Keyspace angelegt haben, können Sie den bekannten Befehl *USE* verwenden: `USE yourNameForThisKeyspace` 

Der Befehl um eine Tabelle anzulegen ist wieder ähnlich zu SQL. Die Datentypen sind aber unterschiedlich! 

```cassandra
CREATE TABLE posts (
    userid text,
    blog_title text,
    posted_at timestamp,
    entry_title text,
    content text,
    category int,
    PRIMARY KEY (userid, blog_title, posted_at)
);
```

Wichtig bei diesem Befehl ist die Anweisung für den Primärschlüssel. Der erste Wert ist immer der Partition Key und die folgenden die Cluster Keys. Wenn mehrere Partition Keys vorhanden sind, werde diese mit Klammern zusammengefasst. 

Beispiel mit einem Partition Key: `PRIMARY KEY (partitionkey1, clusterkey1, clusterkey2)`

Beispiel mit zwei Partition Keys: `PRIMARY KEY ((partitionkey1, partitionkey2), clusterkey1, clusterkey2)`

Tabellen können statische Werte enthalten, welche dann bei jeder Zeile einer Partition gleich ist. Dazu gibt es die Anweisung *static*, die man in der Tabellen-Definition verwenden kann.



## Datenmanipulation (DML)

Die Befehle für die [Datenmanipulation](https://cassandra.apache.org/doc/latest/cassandra/developing/cql/dml.html) haben ähnliche Syntax wie SQL, aber mit deutlichem unterschiedlichem Verhalten. Grob erklärt sind folgende Unterschiede relevant:

**SELECT**: Da in Cassandra der Primärschlüssel in Partition- und Cluster-Keys aufgebaut sind, gibt es Einschränkungen wie man die WHERE-Klausel verwenden darf. Grundsätzlich muss auf die Partition-Keys gefiltert werden und dann auf die Cluster-Keys. Dabei kann kein Cluster-Key übersprungen werden - also die vorhergehenden Cluster-Keys müssen auch im WHERE-Statement vorkommen. Beispiel:

```cassandra
-- Szenario: Primärschlüssel ist wie folgt definiert
PRIMARY KEY (userid, blog_title, posted_at)
-- Ungültige Filterung, weil der Cluster-Key blog_title vor posted_at kommt. 
WHERE userid = 'AB' AND posted_at >= 'XY'
-- Gültige Filterungen sind:
WHERE userid = 'AB' AND blog_title='NM'
WHERE userid = 'AB' AND blog_title='NM' AND posted_at >= 'XY'
```

**INSERT**: Es gibt zusätzlich eine JSON-Syntax, die man verwenden kann. Beispiele finden Sie unter dem Link oben.

**UPDATE**: Updates benötigen in der WHERE-Klausel alle Teile des Primärschlüssels. Man kann also grundsätzlich nicht ein Update aufgrund von anderen Spalten durchführen.

**DELETE**: Mit Delete können auch Spalten gelöscht werden. 

**Batch**: Es gibt eine spezielle Batch-Anweisung, um mehrere Anweisungen auszuführen. Batch sind **keine** Transaktionen und dienen der Effizienz, z.b. ist weniger Netzwerkverkehr notwendig.





