# ACID, CAP und BASE

[TOC]

## ACID

Die Abkürzung ACID steht für die notwendigen Eigenschaften, um Transaktionen auf Datenbankmanagementsystemen (DBMS) einsetzen zu können. Es steht für Atomarität (atomicity), Konsistenz (consistency), Isoliertheit (isolation) und Dauerhaftigkeit (durability).

#### Atomicity (Atomarität)

Ein Block von SQL-Anweisungen (eine oder mehrere) wird entweder ganz oder gar nicht ausgeführt: Das Datenbanksystem behandelt diese Anweisungen wie eine einzelne, unteilbare (daher atomare) Anweisungen. Gibt es bei einem Statement technische Probleme oder tritt ein definierter Zustand ein, der einen Abbruch erfordert (z. B. Kontoüberziehung), werden auch die bereits durchgeführten Operationen nicht auf der Datenbank wirksam.

**Beispiel:** Ein Kunde bestellt via Webshop ein Produkt. Der Warenkorb darf nur geleert werden, wenn gleichzeitig das Produkt bestellt werden kann.  

#### Consistency (Konsistenz)

Nach Abschluss der Transaktion befindet sich die Datenbank in einem konsistenten Zustand. Das bedeutet, dass Widerspruchsfreiheit der Daten gewährleistet sein muss. Das gilt für alle definierten Integritätsbedingungen genauso, wie für die Schlüssel- und Fremdschlüsselverknüpfungen.
Datenbanken erstellen und Daten einfügen Tabellentypen und Transaktionen

**Beispiel:** Ein Kunde verwendet ausschliesslich einen Gutschein als Zahlungsmittel. Sein Warenkorb-Wert darf beim Abschluss der Bestellung den Betrag auf dem Gutschein nicht überschreiten. Dies wäre ein Inkonsistenter Zustand

#### (Isolation) Isoliertheit

Die Ausführungen verschiedener Datenbankmanipulationen dürfen sich nicht gegenseitig beeinflussen. Mittels Sperrkonzepten oder Timestamps wird sichergestellt, dass durch eine Transaktion verwendete Daten nicht vor Beendigung dieser Transaktion verändert werden. Die Sperrungen müssen so kurz und begrenzt wie möglich gehalten werden, da sie die Performance der anderen Operationen beeinflusst.

**Beispiel:** 10 Kunden bestellen gleichzeitig ein Produkt, welches aber nur 9 mal im Lager ist. Es muss sichergestellt sein, dass auch nur 9 Kunden das Produkt bestellen können. 

#### Durability (Dauerhaftigkeit)

Nach Abschluss einer Transaktion müssen die Manipulationen dauerhaft in der Datenbank gespeichert sein. Der Pufferpool speichert häufig verwendete Teile einer Datenbank im Arbeitsspeicher. Diese müssen aber nach Abschluss auf der Festplatte gespeichert werden.

**Beispiel:** Ein Kunde bestellt via Webshop ein Produkt. Während des Bestellprozesses stürzen die Server ab. Der Zustand der Datenbank (und damit auch der der Zustand der Applikation) muss Konsistent bleiben. Es dürfen also keine Informationen verloren gehen.

## CAP-Theorem

Das CAP-Theorem behandelt verteilte System und die drei Eigenschaften Consistency, Availability und Partition tolerance. Das Theorem besagt, dass **nicht alle drei Eigenschaften gleichzeitig erfüllt werden können**.

![CAP](x_res/CAP.png)

#### Consistency (Konsistenz)

Der Begriff der Konsistenz beschreibt, dass jeder Knoten zu jedem Zeitpunkt die gleichen Inhalte enthält, resp. zurückgibt bei einer Anfrage. Die Knoten sind also ständig synchronisiert.

Diese Konsistenz darf nicht verwechselt werden mit der Konsistenz der ACID, die nur die innere Konsistenz eines Datensatzes betrifft.

#### Availability (Verfügbarkeit)

Jeder Knoten, der nicht heruntergefahren ist, muss jederzeit eine Antwort an eine Anfrage zurückgeben. Es geht hier **nicht** darum, dass jeder Knoten ständig up and running ist.

Das System wird nicht als Gesamtes angeschaut, wie z.b. bei einer Web-Applikation bei der es reicht, wenn der Load-Balancer eine Antwort zurück liefert.

#### Partition Tolerance (Partitionstoleranz/Ausfalltoleranz)

Das System arbeitet auch, wenn die Kommunikation zwischen Knoten ausfällt, z.B. durch Verlust (auch kurzzeitige) des Netzwerkes. Ein System gilt als. Ein System gilt als "partitioned", wenn zwei Knoten nicht miteinander kommunizieren können.

#### Auswirkungen des CAP Theorem

Es ist nicht möglich alle drei Eigenschaften in verteilten Systemen gleichzeitig zu erfüllen, sondern immer nur zwei. Wir gehen davon aus, dass es unmöglich ist, dass es nie kurze Unterbrechungen im Netzwerk gibt. Daher ist die P-Eigenschaft bereits vorausgesetzt!

**CP-System**: Gehen Sie nun davon aus, dass die Konsistenz gegeben sein muss, müssen Sie bei einem Netzwerk-Ausfall einzelne Knoten vom Verbund lösen, bis der Netzwerk-Ausfall gelöst ist und die Daten synchronisiert werden konnten. Hier fehlt also die A-Eigenschaft.

![theoreme-CP](x_res/theoreme-CP.png)

**AP-System**: Gehen Sie nun davon aus, dass jederzeit eine sinnvolle Antwort aller laufenden Knoten kommen muss, können Sie davon ausgehen, dass zwei Knoten unterschiedliche Inhalte zurückgeben könnten während eines Netzwerk-Ausfalls zwischen zwei Knoten, z.B. weil während des Ausfalls auf einen Knoten geschrieben wurde.

![theoreme-AP](x_res/theoreme-AP.png)

**AC-System**: Verfügbar und Konsistent ist nur möglich, wenn zwischen zwei Knoten kein Netzwerk besteht oder die Knoten auf dem gleichen Server/Instanz installiert sind. Normalerweise ist ein Single-Node DBMS ein AC-System

#### Weitere Quellen

- Der Beweis, dass nicht alle Eigenschaften gleichzeitig erfüllt werden können [wird hier sehr gut illustriert](https://mwhittaker.github.io/blog/an_illustrated_proof_of_the_cap_theorem/). 
- [Ausführlichere Definition](https://medium.com/@kumar.barmanand/cap-theorem-and-nosql-databases-589e26e15905)



## BASE

BASE steht - wie ACID -  für Konsistenzeigenschaften, welche von NoSQL-Datenbank-Typen verfolgt werden. Im Unterschied zu ACID, sind die Eigenschaften gelockert. 

Der Grund für die Lockerungen ist das **CAP-Theorem** welches **verteilte Systeme** behandelt. **BASE beschreibt die Eigenschaften eines AP-Systems**

#### Basically Available

"Systeme, die auf das BASE-Modell setzen, versprechen, hochverfügbar zu sein und zu jeder Zeit auf Anfragen reagieren zu können. Dafür machen sie Abstriche bei der Konsistenz (siehe dazu auch CAP-Theorem). Das bedeutet, dass ein System immer auf eine Anfrage reagiert. Die Antwort kann aber auch ein Fehler sein oder einen inkonsistenten Datenstand liefern" (wi-wiki.de)

#### Soft State

"Der Zustand, in dem noch nicht alle Änderungen an das gesamte System propagiert wurden, aber trotzdem auf Anfragen reagieren kann, nennt man auch *Soft State*" (wi-wiki.de)

#### Eventually Consistency

"Im Gegensatz zum strengen ACID-Modell wird in BASE die Konsistenz als eine Art „Übergang“ betrachtet, sodass nach einem Update nicht sofort alle Partitionen aktualisiert werden, sondern erst nach einer gewissen Zeit. Das System wird also „**letztendlich konsistent**“ (*eventually consistent*), aber bis dahin in einem inkonsistenten Übergangs-Zustand (*soft state*) sein." (wi-wiki.de)

## Weitere Überlegungen

ACID und BASE stehen nicht in allen Bereichen per se im Gegensatz zueinander. Es können teilweise Eigenschaften von beiden  beide Modelle implementiert werden. Sie müssen aber verstehen wie ihr System im Hintergrund aufgebaut ist (Infrastruktur).

Wenn sie es nicht mit einem verteilten System zu tun haben, können sie auch mit NoSQL ACID einhalten. Tatsächlich [unterstützt MongoDb zum Beispiel Transaktionen](https://www.mongodb.com/basics/acid-transactions).  Sie können daher also jederzeit atomare Abfragen erstellen, die auch die anderen Eigenschaften von ACID erfüllen. 

Sobald ihr System aber auf ein verteiltes System erweitert wird, kann es sein, dass sie nicht die gleichen Daten lesen wie Sie sie eben geschrieben haben. Nun kommt das BASE-Modell zum Einsatz. 

Es geht im BASE-Modell also nicht darum, dass die Daten, die geschrieben werden per se nicht konsistent sind, sondern die Herausforderung steht im Zusammenhang mit einem verteilten System. Es kann sein, dass auf ein System geschrieben wird (konsistent), aber dann später von einem anderen System gelesen wird auf dem die Daten noch nicht aktualisiert wurden.

## Quellen und Weiterführende Literatur

- <https://www.mongodb.com/basics/acid-transactions>
- <http://wi-wiki.de/doku.php?id=bigdata:konsistenz>
- <https://de.wikipedia.org/wiki/CAP-Theorem>
- <https://mwhittaker.github.io/blog/an_illustrated_proof_of_the_cap_theorem/>
