# Prometheus und Grafana Installation

[TOC]

Sie verwenden AWS Ubuntu-Instanzen. Es ist bereits eine Cloud-Init-Konfiguration verfügbar, die Ihnen die Installation abnimmt. Stellen Sie sicher, dass Sie folgende Einstellung in AWS haben:

- Verwenden Sie [dieses Cloud-Init-Skript](cloud-init-prometheus.yaml).
- Ubuntu 24.04
- Verwenden Sie Ihren SSH-Key oder erstellen Sie einen neuen und fügen Sie ihn dem Cloud-Init hinzu. Sie können auch die Keys aus m346 wiederverwenden. Lassen Sie aber den Key für die Lehrperson drin!
- Instanztyp t3.Medium
- Speicherplatz: 20GB
- Fügen Sie eine statische IP hinzu, damit Sie nicht jedes mal Ihre Verbindung anpassen müssen. Dieser Schritt ist wichtig, weil Sie die IP bei Grafana konfigurieren müssen.
- Öffnen Sie die Ports 22, 3000, 9090, 9100



Ob die Installation funktioniert hat, können Sie mit folgenden Befehlen testen

```bash
sudo systemctl status prometheus.service
sudo systemctl status prometheus-node-exporter.service
sudo systemctl status grafana-server
```

Ausserdem sollten die folgenden URLs verfügbar sein:

```
Prometheus Node-Exporter Metrics:
http://<Ihre-IP>:9100/metrics

Prometheus Dashboard:
http://<Ihre-IP>:9090

Grafana Dashboard:
http://<Ihre-IP>:3000
Standard Benutzername ist: admin
Standard Passwort ist: admin

Grafana Metrics für Prometheus
http://<Ihre-IP>:3000/metrics

```

### Quellen

- Installation Prometheus: <https://prometheus.io/docs/prometheus/latest/installation/>
- Prometheus und Cloud-Init: <https://medium.com/@ronaks1907/how-to-set-up-prometheus-using-cloud-init-script-b58a34c3d058>
- Installation Grafana: <https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/>
- Start Grafana: <https://grafana.com/docs/grafana/latest/setup-grafana/start-restart-grafana/>
- Grafana and Prometheus: <https://grafana.com/docs/grafana/latest/datasources/prometheus/configure-prometheus-data-source/>
