# Konzepte TimeSeries Datenbank(en)

[TOC]

## Abgrenzung und Unterschiede

Prometheus fällt in die Kategorie der TimeSeries Datenbanken (TSDB), welche ein Teil von Key-Value-Datenbanken sind. Die verschiedenen TSDBs haben nicht alle den gleichen Zweck und funktionsweise, sondern lösen oft ein spezifisches Problem. Sie können daher nicht einfach eine TSDB mit einer anderen ersetzen.

Als Beispiel nehmen wir zwei der bekanntesten TSDBs, Prometheus und InfluxDB. Während InfluxDB so konzipiert wurde, mit ganz vielen Anfragen umzugehen, erwartet Prometheus im Gegensatz dazu keine aktiven Anfragen von aussen, sondern wird nur periodisch selbst Daten holen. Sie werden in den Übungen mehr dazu erfahren. 

Ein typischer Use Case für InfluxDB ist z.B. das Schreiben von Log-Dateien. Neue Daten können alle paar Millisekunden in die Datenbank geschrieben werden. Im Beispiel im folgenden Bild wird jeder Webseiten-Aufruf in die Datenbank gespeichert. 

Ein typischer Use Case für Prometheus ist das Sammeln von Kennzahlen über eine Periode. Zum Beispiel (im Bild unten) soll alle 5 Minuten gespeichert werden wie oft die verschiedenen Seiten aufgerufen wurden. Diese Information hilft Alarme zu setzen (falls zum Beispiel plötzlich ein Anstieg oder Abfall von Anfragen geschieht).



![TimeSeries](./x_res/TimeSeries.png)



**Quellen**

- [InfluxDB 3.0: System Architektur](https://www.influxdata.com/blog/influxdb-3-0-system-architecture/)
- [Prometheus: Overview](https://prometheus.io/docs/introduction/overview/)
- [InfluxDB vs Prometheus](https://www.influxdata.com/comparison/influxdb-vs-prometheus/)
