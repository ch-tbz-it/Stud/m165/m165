# Neo4j Abfragen, Datenmanipulation

[TOC]

- Cypher: <https://neo4j.com/docs/getting-started/cypher-intro/>
- Queries: <https://neo4j.com/docs/cypher-manual/current/queries/>
- Cypher-Shell: https://neo4j.com/docs/operations-manual/current/tools/cypher-shell/

## Grundsätzliches

Der Primärschlüssel von jedem Dokument ist im Wert `<id>` gespeichert. 

### Verbindung

-  Falls Sie Cypher-Shell verwenden möchten: `cypher-shell -a neo4j://IP-address:7687 -u neo4j -p <password>`
-  Falls Sie Neo4j Desktop verwenden, verwenden Sie den ConnectionString `neo4j://IP-address:7687`. Benutzername und Passwort werden separat abgefragt.

### Abfragen

- `MATCH (n) DETACH DELETE n;` Löscht alle Knoten und Kanten
- `MATCH (n) OPTIONAL MATCH (n)-[r]->(m) RETURN n, r, m;` Gibt **alle** Konten und Kanten zurück



## CRUD Operationen

Da Sie nur Knoten und Kanten haben, sind dies auch die Möglichkeiten Abfragen zu erstellen und Daten anzusprechen. 

### Insert

Verwenden Sie die [CREATE-Klausel](https://neo4j.com/docs/cypher-manual/current/clauses/create/) um Daten einzufügen. Folgend ein paar Beispiele

```cypher
CREATE (); // Dies erstellt einen Knoten ohne Name, Labels und Eigenschaften
CREATE (erster); //Erstellt einen Knoten und speichert eine Referenz in der Variablen 'erster' für die Weiterverarbeitung (siehe unten)
CREATE (zweiter:Abstrakt); // Erstellt einen Knoten und fügt den Label Abstrakt hinzu
CREATE (dritter:Abstrakt:TBZ); // Fügt zwei Labels hinzu
CREATE (vierter:Abstrakt&TBZ); // Fügt ebenfalls zwei Labels hinzu
CREATE (fuenfter:Abstrakt {name: 'Max Muster', age: 44}); // Fügt noch zwei Attribute hinzu

CREATE (:TBZ), (:Abstrakt), (:TBZ:Abstrakt) // Fügt drei Knoten hinzu (ohne Variablen)

CREATE (var1:TBZ)-[var2:isPartOf]->(var3:Abstrakt); // Erstellt eine Kante
CREATE (var1:TBZ), (var2:Abstrakt), (var1)-[:isAlsoPartOf]->(var2); // Erstellt die Kante nachträglich unter Verwendung der Variablen
CREATE (var1:TBZ), (var2:Abstrakt), (var1)-[:isAlsoPartOf {seit: 2021}]->(var2); // fügt der Kante noch zusätzlich Attribute hinzu

// Knoten und Kanten können so beliebig lange verknüpft werden mit nur einer CREATE-Klausel

```

### Read

Um Daten zu lesen können Sie eine ähnliche Syntax verwenden wie beim einfügen von Daten, aber Sie müssen die Elemente auch zurückgeben. Verwenden Sie hauptsächlich die [MATCH-Klausel](https://neo4j.com/docs/cypher-manual/current/clauses/match/).

```cypher
MATCH (n) RETURN n; // Gibt alle Knoten in der Datenbank zurück. 'n' ist die Variable, die für das RETURN Statement verwendet wird.
MATCH (n:person) RETURN n; // Gibt alle Knoten mit einem bestimmten laben zurück
MATCH (n:person:female) RETURN n; // gibt alle Knoten mit BEIDEN labels zurück
MATCH (n:male|female) RETURN n; // gibt alle Knoten mit dem einen ODER dem anderen Label zurück.
MATCH (n:person) RETURN n.name; // Gibt nur die Namen zurück (Tabellenform)
MATCH (n {name: 'Max'}) RETURN n; // Gibt alle Knoten mit einem Attribut-Wert zurück
MATCH (n)-[r]->(m) RETURN n; // Alle Knoten mit AUSGEHENDEN Verbindungen zu beliebigen anderen Knoten
MATCH (n)-[r]->(m) RETURN m; // Alle Knoten mit EINGEHENDEN Verbindungen von beliebigen anderen Knoten
MATCH (n)-[r]->(m) RETURN n,m; // ein- und ausgehende Verbindungen
MATCH (n:male)-[r:kennt]->(m:female) RETURN n; // gibt alle Männer zurück, die Frauen kennen
MATCH (n:male)-[:befreundet|verheiratet]->(m:female) RETURN n; //Alle Männer, die mit Frauen befreundet oder verheiratet sind.
MATCH (n:male)-[r:befreundet|verheiratet]->(m:female) RETURN type(r);

```

### Update

Mit der [SET-Klausel](https://neo4j.com/docs/cypher-manual/current/clauses/set/) und der [REMOVE-Klausel](https://neo4j.com/docs/cypher-manual/current/clauses/remove/) können Sie Attribute und Labels verändern, resp. löschen. Auch hier ist die Syntax wieder ähnlich wie vorher. Sie suchen nach Knoten und Kanten und passen die Werte an

```cypher
MATCH (a {name: 'Max'}) SET a.age = 35; // Ändert ein Attribut eines Knoten
MATCH (a {name: 'Max'}) SET a.age=35, a.height=190 RETURN a; // Ändert zwei Attribute eines Knoten und gibt die Knoten dann zurück
MATCH (n:person {name: 'Max'})-[r:kennt]->(m) SET r.since = 1999; // Ändert ein Attribut einer Kante


```

### Delete

Die [DELETE-Klausel](https://neo4j.com/docs/cypher-manual/current/clauses/delete/) verwenden Sie, um Knoten und Kanten zu löschen. Sie verwenden *Match*, um Elemente zu suchen und löschen diese dann.



```cypher
MATCH (n:person {name: 'Max'}) DELETE n // löscht alle 'n' (Knoten)
MATCH (n:person {name: 'Max'}) DETACH DELETE n // löscht alle 'n' (Knoten) und alle Beziehungen zu und von diesem Knoten
MATCH (n:person {name: 'Max'})-[r:kennt]->() DELETE r // Löscht die Beziehung(en) 'kennt'
MATCH (n:person {name: 'Max'})-[r:kennt]->() DELETE n, r // Löscht die Knoten die Beziehung(en) 'kennt'.
```



## Erweiterte Abfragen

Die bisher genannten Konzepte können nicht alle Abfrage-Arten abdecken. Es gibt noch weitere [Klausel](https://neo4j.com/docs/cypher-manual/current/clauses/), die Individuell entdeckt und ausprobiert werden sollten.

