# Neo4j Installation

[TOC]

Sie können eine der beiden folgenden Varianten wählen (oder auch beide ausführen).

## Cloud Hosting (AuraDB)

Erstellen Sie einen [Account in der Cloud](https://neo4j.com/cloud/aura-free/) und verwenden Sie den für Ihre Aufträge.



## AWS Installation mit Cloud-Init

Sie verwenden AWS Ubuntu-Instanzen. 

Es ist bereits eine Cloud-Init-Konfiguration verfügbar, die Ihnen die Installation abnimmt. Stellen Sie sicher, dass Sie folgende Einstellung in AWS haben:

- Verwenden Sie [dieses Cloud-Init-Skript](cloudinit-neo4j.yaml)
- Ubuntu 24.04 
- Verwenden Sie den gleichen SSH-Key wie Sie für die MongoDB Installation verwendet haben und fügen Sie ihn dem Cloud-Init hinzu. Lassen Sie aber den Key für die Lehrperson drin!
- Kleine Instanz-Typ Grösse (Mikro reicht)
- Speicherplatz: 20GB
- Fügen Sie eine statische IP hinzu, damit Sie nicht jedes mal Ihre Verbindung anpassen müssen.
- Finden Sie raus welche Ports neo4j benötigt und öffnen Sie diese auf Ihrer Instanz.

