# Neo4j Datenmodellierung

[TOC]

## Grundsätzliche Überlegungen

Da wir grundsätzlich von vier verschiedenen Familien von NoSql Datenbank-Typen sprechen, wird es gleich klar, dass es kein einheitliches Konzept zur Datenmodellierung geben kann, welches universell gültig ist. **Allerdings kann das konzeptionelle Schema immer als Grundlage dienen**. Im Folgenden werden mit dem konzeptionelle Diagramm über eine Schule arbeiten.

![konzeptionellesERM](../x_res/ModelSchoolManagement-Konzept.png)



Grundsätzlich können Sie jedes konzeptionelle Modell als Graph darstellen und daher also auch jede relationale Datenbank in eine Graphdatenbank umwandeln.

Das gegebene konzeptionelle Modell könnte man auch anders darstellen. Die Entitäten werden einfach als Knoten abgebildet und die Beziehungen werden zu Kanten (oder eben auch Beziehungen zwischen Knoten).

![logisch1](./x_res/logisch1.png)

Wenn dieses Modell mit einem logischen relationalen Modell verglichen wird, stellt man schnell fest, dass hier die Attribute fehlen. Diese können aber relativ einfach hinzugefügt werden. Wichtig ist zu wissen, dass die Beziehungen hier ebenfalls Attribute und Werte aufweisen können.

![logisch2](./x_res/logisch2.png)

In diesem Beispiel konnten zwei Kanten sogar aufgelöst werden und als Attribut der anderen Kante zugewiesen werden. Es kann durchaus sein, dass mehrere Kanten zwischen zwei Konten notwendig sind, z.B. wenn diese in entgegengesetzte Richtung zeigen.

## Vergleich zu Eigenschaften der Relationalen Datenbanken

### Referenzielle Integrität

Referenzielle Integrität ist bei Graph Datenbanken kein Thema, da auch keine Fremdschlüssel benötigt werden. Die Beziehungen werden direkt mit den Kanten/Beziehungen abgebildet und darum sind keine zusätzlichen Felder bei den Knoten notwendig.

### Primärschlüssel und Fremdschlüssel

Während Primärschlüssel in relationalen Datenbanken (und auch Dokument Datenbanken) notwendig sind, sind dieselben in Graph Datenbanken optional, da die Abfrage oft auch über andere Attribute geschieht. Empfohlen wir, dass die Primärschlüssel, welche zur Identifizierung notwendig sind beibehalten werden (Für die Hauptknoten), aber andere entfernt werden. 

Fremdschlüssel entfallen in Graph Datenbanken, da die Beziehung über den Graph abgebildet werden.

## Modellierungstechniken Neo4j

Neo4j erstellt in Ihren Beispielen nie ein logisches Modell, sondern verwendet immer konkrete Inhalte (z.B. in Attributen) um die Beispiel zu erklären. Dies ist schade, weil die Darstellung von oben schon sehr aussagekräftig ist und speziell die verwendeten Datentypen zeigt. 

### Knoten

Neo4j kennt grundsätzlich einfach nur Knoten. Die Unterscheidung zwischen den verschiedenen Entitäten geschieht über Labels.  Für eine einfachere Visualisierung lassen sich in Neo4j die Labels farblich unterscheiden. **Achtung**: In der folgenden Darstellung sind konkrete Inhalte abgebildet und nicht Definitionen. Im Fenster rechts sieht man die Attribute. Auf dem Knoten selbst wird ein zusätzliches Attribut - das "Caption" angezeigt. 

![data](./x_res/data.png)

### Kanten

Auch Kanten werden unterschieden mit dem Label und auch Kanten können Farben zugewiesen werden. Im folgenden Bild wurde das Beispiel erweitert und verschiedene Farben zugewiesen. Im Fenster rechts sehen Sie die Attribute einer Kante.

![relship](./x_res/relships.png)

### Migration SQL nach Neo4j

Grundsätzlich können folgende Regeln angewandt werden:

- Die Hauptentitäten (aber nicht die Transformationstabellen) werden als Labels der Knoten behandelt
- Der Inhalt einer Hauptentität wird zu einem Knoten mit Attributen. Der eben genannte Label ist notwendig für die Kategoriesierung
- Die Fremdschlüssel-Beziehungen werden mit gerichteten Kanten abgebildet. Die Fremdschlüssel entfallen. Diese Kanten haben oft keine Attribute.
- Die Transformationstabellen werden mit gerichteten oder ungerichteten Kanten abgebildet. Diese Kanten haben oft Attribute, abhängig davon ob die Transformationstabelle Attribute hatte.



## Tools

Verschiedene Tools sind notwendig, wenn Sie komplett graphisch arbeiten möchte. Die Tools können Ihnen  teilweise auch die Cypher-Befehle generieren.

**Datenbankserver**:  Sie haben die Optionen eine AWS Instanz mit einer eigenen Neo4j Datenbank zu installieren oder Sie können den Cloud Provider Aura von Neo4j verwenden für den Sie einen Gratis-Account erstellen können. Sie finden weitere Informationen unter diesem [Link zur Installation](./Installation.md).

**Neo4j Desktop**: [Mit diesem Tool](https://neo4j.com/docs/desktop-manual/current/) können Sie Knoten und Kanten erstellen und auch Abfragen absetzen. Leider können Sie die Knoten und Kanten nicht visuell zeichnen. 

**Arrows.app**: [Mit diesem Tool](https://arrows.app/) können Sie Ihre Knoten graphisch zeichnen und anschliessend als JSON, Cypher-Befehlen und anderen Formaten exportieren. Wenn Sie viele Daten haben, ist es einfacher Sie lernen die Cypher-Syntax als dass Sie alle Kanten und Knoten zeichnen müssen.

**Wichtige Befehle**: Die folgenden Befehle werden Sie benötigen

- Alle Daten löschen: `MATCH (n) DETACH DELETE n;`
- Alle Daten auslesen: `MATCH (n) OPTIONAL MATCH (n)-[r]->(m) RETURN n, r, m;`





## Weitere Quellen

- [Neo4j: Modeling: relational to graph](https://neo4j.com/docs/getting-started/data-modeling/relational-to-graph-modeling/)
- [Neo4j: Graph modeling guidelines](https://neo4j.com/docs/getting-started/data-modeling/guide-data-modeling/) 
