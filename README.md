# m165 - NoSql Datenbanken

### Allgemein

[Wiederholung](./sql.md)

[Infos zu Abgaben](./Abgaben.md)

[ACID / CAP / BASE](./ACID_CAP_BASE.md)

### MongoDB: Document-Store-Datenbank 

[Installation](./MongoDB/Installation.md)

[Modellierung](./MongoDB/Datenmodell.md)

[Datenmanipulation und Datenabfragen](./MongoDB/Abfragen.md)

[Administration](./MongoDB/Administration.md)

### Neo4j: Graph-Datenbank

[Installation](./Neo4j/Installation.md)

[Modellierung](./Neo4j/Datenmodell.md)

[Datenmanipulation und Datenabfragen](./Neo4j/Abfragen.md)

### Cassandra: Wide-Column-Datenbank

[Installation](./Cassandra/Installation.md)

[Modellierung](./Cassandra/Datenmodell.md)

Datenmanipulation und Datenabfragen

### Prometheus: TimeSeries Datenbank

[Konzepte](./Prometheus/Konzepte.md)

[Installation](./Prometheus/Installation.md)

