![TBZ Logo](../x_res/tbz_logo.png)


[TOC]

# KN-C-02: Datenabfrage und -Manipulation

Beachten Sie die [allgemeinen Informationen zu den Abgaben](https://gitlab.com/ch-tbz-it/Stud/m165/m165/-/blob/v2.0/Abgaben.md).

In diesem Kompetenznachweis erstellen Sie eine Cassandra Datenbank und anschliessend das Sie ein Datenmodell für ihr gewähltes Thema. Die Grundlagen können Sie **in der [Theorie](../Cassandra/Abfragen.md) nachlesen**.



## A) Daten hinzufügen (25%)

Fügen Sie eine sinnvolle Menge an Daten hinzu. Im Schnitt sollte dies 3-5 Zeilen sein. Stellen Sie sicher, dass Sie pro Partitions-Key auch mehrere Datensätze haben.

**Abgaben**:

- Eine Skript-Datei (.txt), die die Befehle enthält. Die Datei sollte einfach ausführbar sein, ohne dass  Änderungen notwendig sind. Die Skript-Datei ist so formatiert, dass Sie von Auge lesbar ist (z.B. nicht alles auf einer Zeile)



## B) Daten abfragen (25%)

Sie haben in KN-C-01 Ihre Szenarien definiert. Führen Sie nun die entsprechenden Abfragen aus. 

**Abgaben**:

- Eine Skript-Datei (.txt), die die Befehle enthält. Die Datei sollte einfach ausführbar sein, ohne dass  Änderungen notwendig sind. Die Skript-Datei ist so formatiert, dass Sie von Auge lesbar ist (z.B. nicht alles auf einer Zeile)



## C) Daten löschen (25%)

Löschen Sie einen Teil der Daten raus. Verwenden Sie den Partition-Key um Inhalte zu filtern.

Testen Sie nun die neue Funktion, um Spalten zu löschen. Können Sie Spalten von einzelnen Zeilen löschen? 

Erstellen Sie nun noch eine Skript-Datei, die alle Daten löscht. Diese dient zum aufräumen, so dass Sie Daten wieder hinzufügen können.

**Abgaben**:

- Eine Skript-Datei (.txt), die die Befehle enthält. Die Datei sollte einfach ausführbar sein, ohne dass  Änderungen notwendig sind. Die Skript-Datei ist so formatiert, dass Sie von Auge lesbar ist (z.B. nicht alles auf einer Zeile)
- Eine Skript-Datei (.txt), die alle Daten löscht.



## D) Daten verändern (25%)

Nun werden wir auch Daten aktualisieren. **Beschreiben Sie 3 Szenarios**, die Ihre Themenwelt passt.  Definieren Sie den Anwendungsfall zuerst in Prosa-Text und dann schreiben Sie das *cqlsh*-Statement hinzu. Verzichten Sie auf einfache Abfragen und stellen Sie sich eine Herausforderung.

**Abgaben**:

- Beschreibungen der 3 Szenarien
- Eine Skript-Datei (.txt), die die Befehle enthält. Die Datei sollte einfach ausführbar sein, ohne dass  Änderungen notwendig sind. Die Skript-Datei ist so formatiert, dass Sie von Auge lesbar ist (z.B. nicht alles auf einer Zeile)





