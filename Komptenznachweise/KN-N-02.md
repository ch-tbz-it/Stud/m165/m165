![TBZ Logo](../x_res/tbz_logo.png)


[TOC]

# KN-N-02: Datenabfrage und -Manipulation

Beachten Sie die [allgemeinen Informationen zu den Abgaben](https://gitlab.com/ch-tbz-it/Stud/m165/m165/-/blob/v2.0/Abgaben.md).

In diesem Kompetenznachweis erstellen Sie eine Neo4j Datenbank und anschliessend das Sie ein Datenmodell für ihr gewähltes Thema. Die Grundlagen können Sie **in der [Theorie](../Neo4j/Abfragen.md) nachlesen**.



## A) Daten hinzufügen (20%)

Fügen Sie eine sinnvolle Menge an Knoten und Kanten hinzu. Im Schnitt sollte dies 3-5 Objekte pro Label sein. Versuchen Sie ein einziges grosses CREATE-Statement zu verwenden.

**Abgaben**:

- Eine Skript-Datei (.txt), die die Befehle enthält. Die Datei sollte einfach ausführbar sein, ohne dass  Änderungen notwendig sind. Die Skript-Datei ist so formatiert, dass Sie von Auge lesbar ist (z.B. nicht alles auf einer Zeile)



## B) Daten abfragen (20%)

In der Theorie haben Sie ein Statement gesehen, um alle Knoten und Kanten zu lesen. Erklären Sie das Statement im Detail, speziell auch die noch unbekannte OPTIONAL Klausel.

Nun werden wir auch Daten lesen. **Beschreiben Sie 4 Szenarios**, die Ihre Themenwelt passt.  Definieren Sie den Anwendungsfall zuerst in Prosa-Text und dann schreiben Sie die Abfrage hinzu. Verzichten Sie auf einfache Abfragen und stellen Sie sich eine Herausforderung. Erstellen Sie Statements sowohl für Knoten und Kanten.

Verwenden Sie die WHERE-Klausel in mindestens 2 Statements!



**Abgaben**:

- Erklärung des Statements, um alle Knoten und Kanten zu lesen, speziell die OPTIONAL Klausel
- Beschreibungen der 4 Szenarien und Cypher-Statements dazu.



## C) Daten löschen (20%)

Testen Sie die DETACH-Option. Verwenden Sie zweimal die gleichen Startobjekte (**in Ihrer Themenwelt**) und verwenden Sie einmal DETACH und einmal nicht.  Dokumentieren Sie mit Screenshots das vorher und nachher für beide Fälle.

**Abgaben**:

- Ihre Statements - einmal ohne und einmal mit DETACH.
- Vorher-Nachher Screenshots und Erklärungen dazu.



## D) Daten verändern (20%)

Nun werden wir auch Daten aktualisieren. **Beschreiben Sie 3 Szenarios**, die Ihre Themenwelt passt.  Definieren Sie den Anwendungsfall zuerst in Prosa-Text und dann schreiben Sie das Cypher-Statement hinzu. Verzichten Sie auf einfache Abfragen und stellen Sie sich eine Herausforderung. Erstellen Sie Statements sowohl für Knoten und Kanten

**Abgaben**:

- Beschreibungen der 3 Szenarien und Cypher-Statements dazu.



## E) Zusätzliche Klauseln (20%)

In der [Theorie](../Neo4j/Abfragen.md#erweiterte-abfragen) finden Sie den Link auf alle Cypher-Klauseln. Wählen Sie **zwei neue** Klauseln aus und erklären Sie diese mit eigenen Worten und erstellen Sie Beispiel-Statements dazu in Ihrer Themenwelt und erklären Sie diese. 

**Stellen Sie sicher, dass Sie andere Klauseln wählen als andere Lernende am gleichen Tisch!** Sprechen Sie sich vorher ab.

**Abgaben:**

- Beschreibungen, Erklärungen zum Anwendungsfall (das Beispiel) und Cypher-Statement dazu



