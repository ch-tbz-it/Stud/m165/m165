![TBZ Logo](../x_res/tbz_logo.png)


[TOC]

# KN-N-01: Installation und Datenmodellierung für Neo4j

Beachten Sie die [allgemeinen Informationen zu den Abgaben](https://gitlab.com/ch-tbz-it/Stud/m165/m165/-/blob/v2.0/Abgaben.md).

In diesem Kompetenznachweis erstellen Sie eine Neo4j Datenbank und anschliessend das Sie ein Datenmodell für ihr gewähltes Thema. Die Grundlagen können Sie **in der [Theorie](../Neo4j/Datenmodell.md) nachlesen**.

## A) Installation / Account erstellen (30%)

Folgen Sie der [Installationsanleitung](../Neo4j/Installation.md) und richten Sie Ihre Datenbank ein. 

[Installieren Sie Neo4j Desktop](https://gitlab.com/ch-tbz-it/Stud/m165/m165/-/blob/main/Neo4j/Datenmodell.md#tools). Verbinden Sie sich mit Ihrer Datenbank

**Abgaben:**

- Screenshot der zeigt, dass die Verbindung mit Ihrer Datenbank funktioniert (Aura Cloud Service oder eigne AWS Instanz).

  

## B) Logisches Modell für Neo4j (70%)

Sie haben das konzeptionelle Modell bereits für die MongoDB Aufgaben erstellt. Sie verwenden hier das **gleiche** konzeptionelle Modell als Grundlage. In der [Theorie](../Neo4j/Datenmodell.md) finden Sie die Information wie Sie das das logische Datenmodell erstellen können. 

**Vermeiden Sie konkrete Inhalte und geben Sie Typen von Konten und Kanten an.**

**Abgaben**:

- Ein Bild des logischen Datenmodells.
- Die Original-Datei des logischen Datenmodells (z.B. draw.io)
- Erklärung zu den Attributen und wieso diese auf die entsprechenden Knoten und Kanten verteilt wurden.

