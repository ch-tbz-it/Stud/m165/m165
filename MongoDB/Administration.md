# MongoDB Administration

[TOC]

Die folgenden Themen werden nicht detailliert beschrieben, dafür sind Verlinkungen auf die MongoDB Dokumentation vorhanden. Die kurzen Beschreibungen sollen einen Überblick über die Möglichkeiten geben.



## Built-In Datenbanken

Per Default existieren die folgenden Datenbanken:

- **admin**: Diese Datenbank wird für Benutzer, Rollen und Rechte verwendet. 
- **local** : Wird für die Replikation zwischen mehreren Knoten verwendet und speichert lokale Daten. Diese Daten werden selbst nicht repliziert, sondern speichern den Zustand.
- **config**: Wird verwendet für Partionierung/Sharding und ist neben der Replikation ein anderes System zur Skalierung.



## Rechte und Rollen

Wenn Sie Benutzer erstellen, können Sie [Built-In Rollen](https://www.mongodb.com/docs/manual/reference/built-in-roles/) verwenden, z.b. *read*, *readWrite* oder *dbAdmin*.  Ein Beispiel kann wie folgt aussehen:

```javascript
use admin;
	db.createUser(
	  {
		user: "ihrbenutzer",
		pwd: "ihrpasswort",
		roles: [
		  { role: "userAdminAnyDatabase", db: "admin" }, // Rolle und in welcher DB
		  { role: "readWriteAnyDatabase", db: "admin" }  // Rolle und in welcher DB
		]
	  }
	);
```

Sie können auch [eigene Rollen](https://www.mongodb.com/docs/manual/core/security-user-defined-roles/) erstellen mit spezifischen Zugriffsrechten. Dazu verwenden Sie den Befehl `db.createRole()`.



## Backup und Restore

Es gibt verschiedene Varianten um Backups zu erstellen. Eine gute Variante ist sicherlich die Replikation, die auch für die Ausfallsicherheit und Skalierung sorgen kann. Diese ist relativ komplex umzusetzen. Andere einfachere Arten sind:

- Snapshots der virtuellen Disks. Dies wird nicht weiter beschrieben, weil Sie dies bereits im Modul 346 behandelten. 
- Command-Line-Tool zur Erstellung von Backups: `mongodump` und `mongorestore`. Sie finden weitere Informationen dazu in der offiziellen Dokumentationen:
  - <https://www.mongodb.com/docs/database-tools/mongodump/>
  - <https://www.mongodb.com/docs/database-tools/mongorestore/>



## Skalierung

Skalierung wird über zwei Prinzipien erreicht, die folgend kurz beschrieben werden:

- Replikation: Daten werden über mehrere Knoten repliziert. Jeder Knoten hat ein komplettes Datenset.
- Sharding/Partition: Daten werden aufgeteilt auf mehrere Knoten, so dass die Zuständigkeit eines Knoten nicht über das gesamte Datenset reicht. Dies schliesst Replikation nicht aus!

**Weiterführende Quellen**

<https://www.mongodb.com/basics/scaling>

<https://www.geeksforgeeks.org/mongodb-replication-and-sharding/>
