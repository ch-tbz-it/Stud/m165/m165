# Wiederholung m162

[TOC]

## Konzeptionelles ERM

Die Details [finden Sie im Modul 162](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Datenmodellierung.md?ref_type=heads#konzeptionelles-logisches-und-physisches-datenmodell). Ein **konzeptionelles** Modell ist Datenbankunabhängig und zeigt wie Entitäten zueinander in Beziehung stehen. Das folgende konzeptionelle ERD ist also **auch für NoSql-Datenbanken gültig**. Es zeigt ein Diagramm mit den drei Beziehungstypen hierarchisch, konditionell und netzwerkförmig. 

**Dieses konzeptionelle ERD dient als Basis für die Theorie**. Stellen Sie sicher, sie verstehen es, inklusive allen Kardinalitäten. 

![ModelSchoolManagement-Konzept](./x_res/ModelSchoolManagement-Konzept.png)

## Logisches ERM (Relationale Datenbanken)

Das logische Diagramm wird aufgrund des konzeptionellen Diagramms gebildet und ist typisch für den Datenbank-Typ. Für **relationale Datenbanken** sieht das logische Diagramm wie folgt aus und setzt die folgenden Konzepte um:

- Referenzielle Integrität
- Normalisierung / Vermeidung von Redundanzen.
- ACID Eigenschaften. 

Einige der Konzepte hatten Sie in den Modulen m162 und m164, andere in dem ÜK 106. 

![ModelSchoolManagement-Konzept](./x_res/ModelSchoolManagement-Logisch.png)

